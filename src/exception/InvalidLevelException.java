package exception;

public class InvalidLevelException extends Exception {

    public InvalidLevelException(String s) {
        super(s);
    }

}
