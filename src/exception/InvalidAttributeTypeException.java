package exception;

public class InvalidAttributeTypeException extends Exception {

    public InvalidAttributeTypeException(String s) {
        super(s);
    }

}
