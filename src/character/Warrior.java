package character;

import attribute.AttributeType;
import attribute.PrimaryAttribute;
import attribute.PrimaryAttributeType;
import equipment.Armor;
import equipment.ArmorType;
import equipment.Weapon;
import equipment.WeaponType;
import exception.InvalidArmorException;
import exception.InvalidWeaponException;

public class Warrior extends Hero {

    private static final int VIT_INCREASE_ON_LVLUP = 5, STR_INCREASE_ON_LVLUP = 3, DEX_INCREASE_ON_LVLUP = 2, INT_INCREASE_ON_LVLUP = 1,
            VIT_START_ATTRIBUTE = 10, STR_START_ATTRIBUTE = 5, DEX_START_ATTRIBUTE = 2, INT_START_ATTRIBUTE = 1;

    private static final WeaponType[] ALLOWED_WEAPON_TYPES = {WeaponType.AXE, WeaponType.HAMMER, WeaponType.SWORD};
    private static final ArmorType[] ALLOWED_ARMOR_TYPES = {ArmorType.MAIL, ArmorType.PLATE};

    private final PrimaryAttribute[] START_PRIMARY_ATTRIBUTES = {
            new PrimaryAttribute(PrimaryAttributeType.VITALITY, VIT_START_ATTRIBUTE),
            new PrimaryAttribute(PrimaryAttributeType.STRENGTH, STR_START_ATTRIBUTE),
            new PrimaryAttribute(PrimaryAttributeType.DEXTERITY, DEX_START_ATTRIBUTE),
            new PrimaryAttribute(PrimaryAttributeType.INTELLIGENCE, INT_START_ATTRIBUTE)
    };

    public Warrior(String name) {
        super(name, PrimaryAttributeType.STRENGTH, new PrimaryAttribute[]{
                new PrimaryAttribute(PrimaryAttributeType.VITALITY, VIT_INCREASE_ON_LVLUP),
                new PrimaryAttribute(PrimaryAttributeType.STRENGTH, STR_INCREASE_ON_LVLUP),
                new PrimaryAttribute(PrimaryAttributeType.DEXTERITY, DEX_INCREASE_ON_LVLUP),
                new PrimaryAttribute(PrimaryAttributeType.INTELLIGENCE, INT_INCREASE_ON_LVLUP)
        });
        initPrimaryAttributes(START_PRIMARY_ATTRIBUTES);
        initSecondaryAttributes();
    }

    @Override
    protected boolean canEquipArmor(Armor armor) throws InvalidArmorException {
        for (int i = 0; i < ALLOWED_ARMOR_TYPES.length; i++) {
            if (armor.getType() == ALLOWED_ARMOR_TYPES[i]) return true;
        }
        throw new InvalidArmorException("Armor type not allowed");
    }

    @Override
    protected boolean canEquipWeapon(Weapon weapon) throws InvalidWeaponException {
        for (int i = 0; i < ALLOWED_WEAPON_TYPES.length; i++) {
            if (weapon.getType() == ALLOWED_WEAPON_TYPES[i]) return true;
        }
        throw new InvalidWeaponException("Weapon type not allowed");
    }

}
