package character;

import attribute.AttributeType;
import attribute.PrimaryAttribute;
import attribute.PrimaryAttributeType;
import equipment.Armor;
import equipment.ArmorType;
import equipment.Weapon;
import equipment.WeaponType;
import exception.InvalidArmorException;
import exception.InvalidWeaponException;

public class Mage extends Hero {

    private static final int VIT_INCREASE_ON_LVLUP = 3, STR_INCREASE_ON_LVLUP = 1, DEX_INCREASE_ON_LVLUP = 1, INT_INCREASE_ON_LVLUP = 5,
            VIT_START_ATTRIBUTE = 5, STR_START_ATTRIBUTE = 1, DEX_START_ATTRIBUTE = 1, INT_START_ATTRIBUTE = 8;

    private static final WeaponType[] ALLOWED_WEAPON_TYPES = {WeaponType.STAFF, WeaponType.WAND};
    private static final ArmorType ALLOWED_ARMOR_TYPE = ArmorType.CLOTH;

    private final PrimaryAttribute[] START_PRIMARY_ATTRIBUTES = {
            new PrimaryAttribute(PrimaryAttributeType.VITALITY, VIT_START_ATTRIBUTE),
            new PrimaryAttribute(PrimaryAttributeType.STRENGTH, STR_START_ATTRIBUTE),
            new PrimaryAttribute(PrimaryAttributeType.DEXTERITY, DEX_START_ATTRIBUTE),
            new PrimaryAttribute(PrimaryAttributeType.INTELLIGENCE, INT_START_ATTRIBUTE)
    };

    public Mage(String name) {
        super(name, PrimaryAttributeType.INTELLIGENCE, new PrimaryAttribute[]{
                new PrimaryAttribute(PrimaryAttributeType.VITALITY, VIT_INCREASE_ON_LVLUP),
                new PrimaryAttribute(PrimaryAttributeType.STRENGTH, STR_INCREASE_ON_LVLUP),
                new PrimaryAttribute(PrimaryAttributeType.DEXTERITY, DEX_INCREASE_ON_LVLUP),
                new PrimaryAttribute(PrimaryAttributeType.INTELLIGENCE, INT_INCREASE_ON_LVLUP)
        });
        initPrimaryAttributes(START_PRIMARY_ATTRIBUTES);
        initSecondaryAttributes();
    }

    @Override
    protected boolean canEquipArmor(Armor armor) throws InvalidArmorException {
        if (armor.getRequiredLevel() > this.getLevel())
            throw new InvalidArmorException("Level too low to equip this item.");
        else if (armor.getType() == ALLOWED_ARMOR_TYPE) return true;
        throw new InvalidArmorException("Armor type not allowed.");
    }

    @Override
    protected boolean canEquipWeapon(Weapon weapon) throws InvalidWeaponException {
        for (int i = 0; i < ALLOWED_WEAPON_TYPES.length; i++) {
            if (weapon.getType() == ALLOWED_WEAPON_TYPES[i]) return true;
        }
        throw new InvalidWeaponException("Weapon type not allowed");
    }

}
