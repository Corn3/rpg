package character;

import attribute.*;
import equipment.*;
import exception.*;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicReference;

public abstract class Hero {

    private final String name;
    private int level;
    private final PrimaryAttributeType mainPrimaryAttribute;

    private PrimaryAttribute[] basePrimaryAttributes;
    private PrimaryAttribute[] totalPrimaryAttributes;
    private final PrimaryAttribute[] attributesOnLevelUp;
    private SecondaryAttribute[] secondaryAttributes;
    private HashMap<Slot, Equipment> equippedItems = new HashMap<>();

    private final HeroAttributeCalculator heroAttributeCalculator;

    public Hero(String name, PrimaryAttributeType mainPrimaryAttribute, PrimaryAttribute[] attributesOnLevelUp) {
        this.name = name;
        this.mainPrimaryAttribute = mainPrimaryAttribute;
        this.attributesOnLevelUp = attributesOnLevelUp;
        level = 1;
        heroAttributeCalculator = new HeroAttributeCalculator(this);
    }

    /**
     * Increases the number of levels this hero has by the given argument.
     * Upon leveling up this hero increases its base, total and secondary
     * attributes based on a predefined value and uses level as a multiplier.
     *
     * @param levels the number of levels to be applied to the current levels.
     * @throws ArgumentException Gets thrown when the number of levels is lower than one.
     */
    public void levelUp(int levels) throws ArgumentException {
        increaseLevel(levels);
        for(int i = 1; i <= levels; i++){
            heroAttributeCalculator.increaseBaseStats(attributesOnLevelUp);
            heroAttributeCalculator.increaseTotalStats(attributesOnLevelUp);
        }
        for(int i = 0; i < secondaryAttributes.length; i++)
            heroAttributeCalculator.calculateSecondaryAttributes(secondaryAttributes[i]);
    }

    /**
     * Checks if it is possible to equip the armor piece from the argument.
     * <p>
     * Always returns if this hero can equip the armor piece and if not then
     * a exception gets thrown.
     *
     * @param armor the armor piece to be checked.
     * @return True is returned if this hero can equip this piece.
     * @throws InvalidArmorException is thrown when the hero is unable to equip the armor, either due to
     * wrong slot or wrong type.
     */
    protected abstract boolean canEquipArmor(Armor armor) throws InvalidArmorException;

    /**
     * Checks if it is possible to equip the weapon piece from the argument.
     * <p>
     * Always returns if this hero can equip the weapon piece and if not then
     * a exception gets thrown.
     *
     * @param weapon the weapon piece to be checked.
     * @return True is returned if this hero can equip this piece.
     * @throws InvalidWeaponException is thrown when the hero is unable to equip the weapon, either due to
     * wrong slot or wrong type.
     */
    protected abstract boolean canEquipWeapon(Weapon weapon) throws InvalidWeaponException;

    /**
     * Initializes this hero with primary attributes based on this hero's class type
     * and sets the total attributes to the same value. t
     *
     * @param basePrimaryAttributes the specific primary attributes that this character starts with
     */
    protected void initPrimaryAttributes(PrimaryAttribute[] basePrimaryAttributes) {
        this.basePrimaryAttributes = basePrimaryAttributes;
        totalPrimaryAttributes = new PrimaryAttribute[basePrimaryAttributes.length];
        for(int i = 0; i < totalPrimaryAttributes.length; i++) {
            totalPrimaryAttributes[i] = new PrimaryAttribute((PrimaryAttributeType)basePrimaryAttributes[i].getType(),
                    basePrimaryAttributes[i].getAmount());
        }
    }

    /**
     * Initializes this hero with secondary attributes based on this hero's primary attributes.
     *
     */
    protected void initSecondaryAttributes() {
        secondaryAttributes = new SecondaryAttribute[]{
                new Health(getSpecificTotalStatsAmount(PrimaryAttributeType.VITALITY).getAmount()),
                new ArmorRating(getSpecificTotalStatsAmount(PrimaryAttributeType.STRENGTH).getAmount(),
                                getSpecificTotalStatsAmount(PrimaryAttributeType.DEXTERITY).getAmount()),
                new ElementalResistance(getSpecificTotalStatsAmount(PrimaryAttributeType.INTELLIGENCE).getAmount()),
                new Damage(getSpecificTotalStatsAmount(getMainPrimaryAttribute()).getAmount(), 1)
        };
    }

    /**
     * Fetches the total primary attribute value for this hero based on the
     * attribute type argument.
     * <p>
     * This method always returns when the given attribute type is found
     * else it returns 0.
     *
     * @param type the name of the attribute from which to fetch the value
     * @return the total value for the specified attribute
     */
    public PrimaryAttribute getSpecificTotalStatsAmount(PrimaryAttributeType type) {
        for (PrimaryAttribute totalPrimaryAttribute : totalPrimaryAttributes)
            if (totalPrimaryAttribute.getType() == type)
                return totalPrimaryAttribute;

        return null;
    }

    public PrimaryAttribute getSpecificBaseStatsAmount(PrimaryAttributeType type) {
        for (PrimaryAttribute primaryAttribute : basePrimaryAttributes)
            if (primaryAttribute.getType() == type)
                return primaryAttribute;

        return null;
    }

    public PrimaryAttributeType getMainPrimaryAttribute() {
        return mainPrimaryAttribute;
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public SecondaryAttribute getSpecificSecondaryAttributes(AttributeType type) {
        for (int i = 0; i < totalPrimaryAttributes.length; i++)
            if (secondaryAttributes[i].getType() == type)
                return secondaryAttributes[i];

        return null;
    }

    public Weapon getEquippedWeapon() {
        return (Weapon)equippedItems.get(Slot.WEAPON);
    }

    /**
     * Removes the equipment based on the argument and changes the attributes
     * back to their base value.
     * <p>
     * The method always returns the removed equipment piece (This could be
     * used if an inventory existed).
     *
     * @param slot the slot in which an item to be unequipped from is located
     * @return the removed equipment piece
     */
    private Equipment unEquipEquipmentInSlot(Slot slot) {
        // Recalculate based on the unequipped armor piece instead of resetting to baseStats.
        Equipment removedEquipment = equippedItems.remove(slot);
        totalPrimaryAttributes = basePrimaryAttributes;
        return removedEquipment;
    }

    /**
     * Increases the number of levels this hero has based on the level argument.
     *
     * @param level the number of levels to increase this hero's level with
     * @throws ArgumentException gets thrown when 0 or less levels are specified
     */
    protected void increaseLevel(int level) throws ArgumentException {
        if (level >= 1)
            this.level += level;
        else
            throw new ArgumentException("Level gain must be greater than 0");
    }

    /**
     * Equips an armor piece, while removing the item in the armor's equipped slot.
     * Recalculates all attributes when this process is done.
     * <p>
     * This method always returns true if the equipping process was successful.
     *
     * @param armor the equipment piece to be equipped
     * @return A boolean is returned when the item is equipped.
     * @throws InvalidLevelException If this hero's level is too low to equip this item
     * @throws InvalidArmorException Gets thrown when the armor is attempted to be
     * equipped in the weapon slot or when this hero can't equip that armor type.
     */
    public boolean equip(Armor armor) throws InvalidLevelException, InvalidArmorException {
        if (this.isRequiredLevel(armor) && this.canEquipArmor(armor)) {
            if (equippedItems.get(armor.getSlot()) != null) {
                unEquipEquipmentInSlot(armor.getSlot());
            }
            equippedItems.put(armor.getSlot(), armor);
            heroAttributeCalculator.increaseTotalStats(armor.getPrimaryAttributes());
            for(SecondaryAttribute attribute : secondaryAttributes) {
                heroAttributeCalculator.calculateSecondaryAttributes(attribute);
            }
        }
        return true;
    }

    /**
     * Equips a weapon piece, while removing the item in the weapon's equipped slot.
     * Recalculates all attributes when this process is done.
     * <p>
     * This method always returns true if the equipping process was successful.
     *
     * @param weapon the equipment piece to be equipped
     * @return A boolean is returned when the item is equipped.
     * @throws InvalidLevelException If this hero's level is too low to equip this item.
     * @throws InvalidWeaponException Gets thrown when the weapon is attempted to be
     * equipped in one of the armor slots or when this hero can't equip that weapon type.
     */
    public boolean equip(Weapon weapon) throws InvalidLevelException, InvalidWeaponException {
        if (this.isRequiredLevel(weapon) && this.canEquipWeapon(weapon)) {
            if (equippedItems.get(weapon.getSlot()) != null) {
                unEquipEquipmentInSlot(weapon.getSlot());
            }
            equippedItems.put(weapon.getSlot(), weapon);
            heroAttributeCalculator.calculateSecondaryAttributes(getSpecificSecondaryAttributes(SecondaryAttributeType.DPS));
        }
        return true;
    }

    /**
     * Checks if this hero meets the required level to equip this piece of equipment
     * passed as an argument.
     * @param equipment The piece of equipment to be checked for a correct level.
     * @return A true value is returned if this hero meet the required level
     * @throws InvalidLevelException This exception is thrown when this hero has too low
     * of a level to equip.
     */
    protected boolean isRequiredLevel(Equipment equipment) throws InvalidLevelException {
        if (equipment.getRequiredLevel() > this.getLevel())
            throw new InvalidLevelException("Level is too low for this equipment.");
        else
            return true;
    }

    /**
     * Retrieves the total armor for this hero's equipped armor pieces.
     *
     * @return The total armor value from the equipped armor pieces.
     */
    public double getTotalEquippedArmor() {
        AtomicReference<Double> armorAmount = new AtomicReference<>((double) 0);

        equippedItems.forEach((k, v) -> {
            if(v instanceof Armor armor) {
                armorAmount.set(armor.getArmorValue());
            }

        });
        return armorAmount.get();
    }


}
