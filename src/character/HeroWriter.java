package character;

import attribute.AttributeType;
import attribute.PrimaryAttributeType;
import attribute.SecondaryAttribute;
import attribute.SecondaryAttributeType;
import exception.InvalidAttributeTypeException;

public class HeroWriter {

    private Hero hero;

    public HeroWriter(Hero hero) {
        this.hero = hero;
    }

    /**
     * Displays the character's attributes. By using a StringBuilder,
     * this writer builds a String from the character's specific data
     * and its attributes. This writer then prints the constructed
     * String to the console.
     */
    public void displayStatsOnConsole() {
        StringBuilder display = new StringBuilder();
        addCharacterSpecificDataToDisplay(display);
        addCharacterPrimaryAttributesToDisplay(display);
        addCharacterSecondaryAttributesToDisplay(display);
        System.out.println(display);
    }

    /**
     * Concatenates all the information that are specific to this character
     * and adds them to the StringBuilder object.
     *
     * @param display the StringBuilder used for constructing the character stats display
     */
    private void addCharacterSpecificDataToDisplay(StringBuilder display) {
        display.append("Name: " + hero.getName() + "\nLevel: " + hero.getLevel() + "\nClass: " + hero.getClass().getSimpleName());
    }

    /**
     * Adds the primary attribute values to the StringBuilder object.
     *
     * @param display the StringBuilder used for constructing the character stats display
     */
    private void addCharacterPrimaryAttributesToDisplay(StringBuilder display) {
        for (PrimaryAttributeType type : PrimaryAttributeType.values()) {
            display.append("\n" + hero.getSpecificTotalStatsAmount(type));
        }
    }

    /**
     * Adds the secondary attribute values to the StringBuilder object.
     *
     * @param display the StringBuilder used for constructing the character stats display
     */
    private void addCharacterSecondaryAttributesToDisplay(StringBuilder display) {
        for (SecondaryAttributeType attributeType: SecondaryAttributeType.values()) {
            String str;
            SecondaryAttribute attribute = hero.getSpecificSecondaryAttributes(attributeType);
            if (attributeType == SecondaryAttributeType.ARMOR_RATING) {
                double armor = hero.getTotalEquippedArmor();
                if (armor > 0)
                    str = "\n" + attribute + " + " + armor;
                else
                    str = "\n" + attribute;
            } else
                str = "\n" + attribute;
            display.append(str);
        }

    }
}
