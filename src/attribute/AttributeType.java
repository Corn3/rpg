package attribute;

public interface AttributeType {

    String getAttributeName();

    AttributeType getAttributeType();

}
