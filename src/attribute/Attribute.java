package attribute;

public abstract class Attribute {

    private AttributeType type;
    private double amount;

    public Attribute(AttributeType type, double amount) {
        this.type = type;
        this.amount = amount;
    }

    public AttributeType getType() {
        return type.getAttributeType();
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return type.getAttributeName().substring(0, 1) + type.getAttributeName().substring(1).toLowerCase().replace("_", " ") + ": " + amount;
    }

}
