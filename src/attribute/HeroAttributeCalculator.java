package attribute;

import character.Hero;
import exception.InvalidAttributeTypeException;

public class HeroAttributeCalculator {

    private Hero hero;

    public HeroAttributeCalculator(Hero hero) {
        this.hero = hero;
    }

    /**
     * Increases this hero's base primary attributes. The argument must specify the attribute values
     * received when this hero gains a new level.
     *
     * @param attributes the primary attribute values used to increase the base attributes
     */
    public void increaseBaseStats(PrimaryAttribute[] attributes) {
        for (int i = 0; i < PrimaryAttributeType.values().length; i++) {
            hero.getSpecificBaseStatsAmount(PrimaryAttributeType.values()[i]).
                    setAmount(hero.getSpecificBaseStatsAmount(
                            PrimaryAttributeType.values()[i]).getAmount() + attributes[i].getAmount());
        }
    }

    /**
     * Increases this hero's total primary attributes. The argument must specify the attribute values
     * received when this hero gains a new level.
     *
     * @param primaryAttributes the primary attribute values used to increase the total attributes
     */
    public void increaseTotalStats(PrimaryAttribute[] primaryAttributes) {
        for (int i = 0; i < PrimaryAttributeType.values().length; i++) {
            hero.getSpecificTotalStatsAmount(PrimaryAttributeType.values()[i]).setAmount(
                    hero.getSpecificTotalStatsAmount(
                            PrimaryAttributeType.values()[i]).getAmount() + primaryAttributes[i].getAmount());
        }
    }

    /**
     * Changes the secondary attributes for this hero based on the total primary attributes.
     */
    public void calculateSecondaryAttributes(SecondaryAttribute attribute) {
        switch ((SecondaryAttributeType) attribute.getType()) {
            case HEALTH:
                attribute.calculateValueBasedOnAttributes(
                        hero.getSpecificTotalStatsAmount(PrimaryAttributeType.VITALITY).getAmount());
                break;
            case ARMOR_RATING:
                attribute.calculateValueBasedOnAttributes(
                        hero.getSpecificTotalStatsAmount(PrimaryAttributeType.STRENGTH).getAmount(),
                        hero.getSpecificTotalStatsAmount(PrimaryAttributeType.DEXTERITY).getAmount());
                break;
            case ELEMENTAL_RESISTANCE:
                attribute.calculateValueBasedOnAttributes(
                        hero.getSpecificTotalStatsAmount(PrimaryAttributeType.INTELLIGENCE).getAmount());
                break;
            case DPS:
                double weaponDps = (hero.getEquippedWeapon() == null) ? 1 : hero.getEquippedWeapon().getDPSValue();
                attribute.calculateValueBasedOnAttributes(
                        hero.getSpecificTotalStatsAmount(hero.getMainPrimaryAttribute()).getAmount(), weaponDps);
                break;
        }
    }

}


