package attribute;

public enum PrimaryAttributeType implements AttributeType {
    VITALITY,
    STRENGTH,
    DEXTERITY,
    INTELLIGENCE;

    @Override
    public String getAttributeName() {
        return this.name();
    }

    @Override
    public AttributeType getAttributeType() {
        return this;
    }
}
