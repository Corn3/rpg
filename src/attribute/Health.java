package attribute;

public class Health extends SecondaryAttribute {

    public Health(double amount) {
        super(SecondaryAttributeType.HEALTH, amount * 10);
    }

    @Override
    public double calculateValueBasedOnAttributes(double... values) {
        if(values.length != 1)
            throw new IllegalArgumentException("You need to specify one value, and that is for the vitality value.");
        double value = values[0] * 10;
        this.setAmount(value);
        return value;
    }
}
