package attribute;

public class Damage extends SecondaryAttribute {
    public Damage(double mainAttribute, double weaponDPS) {
        super(SecondaryAttributeType.DPS, weaponDPS * (1 + mainAttribute / 100));
    }

    //Generic types should probably be used instead of double (and pass the attributes), as a check for correctly passed attributes is needed.
    @Override
    public double calculateValueBasedOnAttributes(double... values) {
        if(values.length != 2) {
            throw new IllegalArgumentException("You need to specify two values, one for the main primary attribute and one for weapon dps in that order.");
        }

        double mainAttribute = values[0], weaponDPS = values[1], value;
        value = weaponDPS * (1 + mainAttribute / 100);
        this.setAmount(value);
        return value;
    }
}
