package attribute;

public class ArmorRating extends SecondaryAttribute {

    public ArmorRating(double strength, double dexterity) {
        super(SecondaryAttributeType.ARMOR_RATING, strength + dexterity);
    }

    @Override
    public double calculateValueBasedOnAttributes(double ... values) {
        if(values.length != 2) {
            throw new IllegalArgumentException("You need to specify two values, one for strength value and one for dexterity.");
        }

        double value = 0;
        for(double i : values) {
            value += i;
        }
        this.setAmount(value);
        return value;
    }

}
