package attribute;

public enum SecondaryAttributeType implements AttributeType {
    HEALTH,
    ARMOR_RATING,
    ELEMENTAL_RESISTANCE,
    DPS;

    @Override
    public String getAttributeName() {
        return this.name();
    }

    @Override
    public AttributeType getAttributeType() {
        return this;
    }
}
