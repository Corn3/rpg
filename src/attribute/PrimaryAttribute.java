package attribute;

public class PrimaryAttribute extends Attribute{

    public PrimaryAttribute(PrimaryAttributeType type, double amount) {
        super(type, amount);
    }

}
