package attribute;

public abstract class SecondaryAttribute extends Attribute {

    public SecondaryAttribute(SecondaryAttributeType type, double amount) {
        super(type, amount);
    }


    /**
     * Calculates the value for this secondary attribute based on 1 or more arguments.
     * The argument can be anything from primary attribute- to secondary attribute values.
     * <p>
     * This method always returns the result of the calculation or throws an exception if
     * the number of arguments is wrong.
     * 
     * @param values is used for calculating the amount this attribute has.
     * @return the result from the calculation.
     * @throws IllegalArgumentException is thrown when the number of arguments is too low or high
     * in the inherited class.
     */
    public abstract double calculateValueBasedOnAttributes(double ... values) throws IllegalArgumentException;

}
