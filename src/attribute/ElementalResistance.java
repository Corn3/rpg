package attribute;

public class ElementalResistance extends SecondaryAttribute {

    public ElementalResistance(double amount) {
        super(SecondaryAttributeType.ELEMENTAL_RESISTANCE, amount);
    }

    @Override
    public double calculateValueBasedOnAttributes(double... values) {
        if(values.length != 1)
            throw new IllegalArgumentException("You need to specify one value, and that is for intelligence value.");
        this.setAmount(values[0]);
        return values[0];

    }
}
