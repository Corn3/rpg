package equipment;

import attribute.PrimaryAttribute;
import exception.InvalidArmorException;

public class Armor extends Equipment {

    private int armorValue;
    private ArmorType type;
    private PrimaryAttribute[] primaryAttributes;

    public Armor(String name, int requiredLevel, Slot slot, ArmorType type, int armorValue, PrimaryAttribute[] primaryAttributes) throws InvalidArmorException {
        super(name, requiredLevel, slot);
        if(slot == Slot.WEAPON)
            throw new InvalidArmorException("Can't equip armor in slot " + slot.name());
        this.type = type;
        this.armorValue = armorValue;
        this.primaryAttributes = primaryAttributes;
    }

    public double getArmorValue() {
        return armorValue;
    }

    public ArmorType getType() {
        return type;
    }

    public PrimaryAttribute[] getPrimaryAttributes() {
        return primaryAttributes;
    }

}
