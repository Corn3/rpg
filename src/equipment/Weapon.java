package equipment;

import attribute.Damage;
import attribute.SecondaryAttribute;
import exception.InvalidWeaponException;

public class Weapon extends Equipment {

    private SecondaryAttribute dps;
    private int damageValue;
    private double attackSpeed;
    private WeaponType type;

    public Weapon(String name, int requiredLevel, Slot slot, WeaponType type, int damageValue, double attackSpeed) throws InvalidWeaponException {
        super(name, requiredLevel, slot);
        if(slot != Slot.WEAPON)
            throw new InvalidWeaponException("Can't equip weapon in slot " + slot.name());
        this.type = type;
        this.damageValue = damageValue;
        this.attackSpeed = attackSpeed;
        double dpsValue = damageValue * attackSpeed;
        dps = new Damage(0, dpsValue);
    }

    public SecondaryAttribute getDps() {
        return dps;
    }

    public int getDamageValue() {
        return damageValue;
    }

    public double getAttackSpeed() {
        return attackSpeed;
    }

    public WeaponType getType() {
        return type;
    }

    public double getDPSValue() {
        return dps.getAmount();
    }

}
