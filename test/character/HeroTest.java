package character;

import exception.ArgumentException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HeroTest {

    private Hero character;

    @BeforeEach
    public void initializeCharacter() {
        character = new Mage("Jaina");
    }

    @Test
    public void checkCorrectStartingLevel() {
        assertEquals(1, character.getLevel());
    }

    @Test
    public void checkCorrectLevelOnLevelUp() throws ArgumentException {
        character.levelUp(1);
        assertEquals(2, character.getLevel());
    }

    @Test
    public void levelUp_zeroOrLess_throwsArgumentException() {
        assertThrows(ArgumentException.class, () -> {
            character.levelUp(0);
            character.levelUp(-1);
        });
    }

}