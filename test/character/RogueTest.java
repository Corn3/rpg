package character;

import attribute.PrimaryAttributeType;
import exception.ArgumentException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RogueTest {

    private static final int BASE_VITALITY = 8, BASE_STRENGTH = 2, BASE_DEXTERITY = 6, BASE_INTELLIGENCE = 1,
            LEVELUP_VITALITY = 3, LEVELUP_STRENGTH = 1, LEVELUP_DEXTERITY = 4, LEVELUP_INTELLIGENCE = 1;
    private static final double[] EXPECTED_BASE_ATTRIBUTES = {BASE_VITALITY, BASE_STRENGTH, BASE_DEXTERITY, BASE_INTELLIGENCE};
    private static final double[] EXPECTED_LEVELUP_ATTRIBUTES = {
            BASE_VITALITY + LEVELUP_VITALITY,
            BASE_STRENGTH + LEVELUP_STRENGTH,
            BASE_DEXTERITY + LEVELUP_DEXTERITY,
            BASE_INTELLIGENCE + LEVELUP_INTELLIGENCE
    };

    private Hero rogue;

    @BeforeEach
    public void initializeCharacter() {
        rogue = new Rogue("Vanessa");
    }

    private double[] getPrimaryAttributes() {
        double[] primaryAttributes = new double[PrimaryAttributeType.values().length];
        for (int i = 0; i < primaryAttributes.length; i++) {
            primaryAttributes[i] = rogue.getSpecificBaseStatsAmount(PrimaryAttributeType.values()[i]).getAmount();
        }
        return primaryAttributes;
    }

    @Test
    public void checkCorrectStartingPrimaryAttributes() {
        double[] primaryAttributes = getPrimaryAttributes();
        assertArrayEquals(EXPECTED_BASE_ATTRIBUTES, primaryAttributes);
    }

    @Test
    public void checkCorrectStatsOnLevelUp() throws ArgumentException {
        rogue.levelUp(1);
        double[] primaryAttributes = getPrimaryAttributes();
        assertArrayEquals(EXPECTED_LEVELUP_ATTRIBUTES, primaryAttributes);
    }

}