package character;

import attribute.PrimaryAttribute;
import attribute.PrimaryAttributeType;
import exception.ArgumentException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MageTest {

    private static final int BASE_VITALITY = 5, BASE_STRENGTH = 1, BASE_DEXTERITY = 1, BASE_INTELLIGENCE = 8,
            LEVELUP_VITALITY = 3, LEVELUP_STRENGTH = 1, LEVELUP_DEXTERITY = 1, LEVELUP_INTELLIGENCE = 5;
    private static final double[] EXPECTED_BASE_ATTRIBUTES = {BASE_VITALITY, BASE_STRENGTH, BASE_DEXTERITY, BASE_INTELLIGENCE};
    private static final double[] EXPECTED_LEVELUP_ATTRIBUTES = {
            BASE_VITALITY + LEVELUP_VITALITY,
            BASE_STRENGTH + LEVELUP_STRENGTH,
            BASE_DEXTERITY + LEVELUP_DEXTERITY,
            BASE_INTELLIGENCE + LEVELUP_INTELLIGENCE
    };

    private Hero mage;

    @BeforeEach
    public void initializeCharacter() {
        mage = new Mage("Jaina");
    }

    private double[] getPrimaryAttributes() {
        double[] primaryAttributes = new double[PrimaryAttributeType.values().length];
        for (int i = 0; i < primaryAttributes.length; i++) {
            primaryAttributes[i] = mage.getSpecificBaseStatsAmount(PrimaryAttributeType.values()[i]).getAmount();
        }
        return primaryAttributes;
    }

    @Test
    public void checkCorrectStartingPrimaryAttributes() {
        double[] primaryAttributes = getPrimaryAttributes();
        assertArrayEquals(EXPECTED_BASE_ATTRIBUTES, primaryAttributes);
    }

    @Test
    public void checkCorrectStatsOnLevelUp() throws ArgumentException {
        mage.levelUp(1);
        double[] primaryAttributes = getPrimaryAttributes();
        assertArrayEquals(EXPECTED_LEVELUP_ATTRIBUTES, primaryAttributes);
    }

}