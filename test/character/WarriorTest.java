package character;

import attribute.PrimaryAttributeType;
import attribute.SecondaryAttributeType;
import exception.ArgumentException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WarriorTest {

    private static final int BASE_VITALITY = 10, BASE_STRENGTH = 5, BASE_DEXTERITY = 2, BASE_INTELLIGENCE = 1,
            LEVELUP_VITALITY = 5, LEVELUP_STRENGTH = 3, LEVELUP_DEXTERITY = 2, LEVELUP_INTELLIGENCE = 1;
    private static final double[] EXPECTED_BASE_ATTRIBUTES = {BASE_VITALITY, BASE_STRENGTH, BASE_DEXTERITY, BASE_INTELLIGENCE};
    private static final double[] EXPECTED_LEVELUP_ATTRIBUTES = {
            BASE_VITALITY + LEVELUP_VITALITY,
            BASE_STRENGTH + LEVELUP_STRENGTH,
            BASE_DEXTERITY + LEVELUP_DEXTERITY,
            BASE_INTELLIGENCE + LEVELUP_INTELLIGENCE
    };
    private static final double[] EXPECTED_LEVELUP_SECONDARY_ATTRIBUTES = {
            150,
            12,
            2
    };

    private Hero warrior;

    @BeforeEach
    public void initializeCharacter() {
        warrior = new Warrior("Varian");
    }

    private double[] getPrimaryAttributes() {
        double[] primaryAttributes = new double[PrimaryAttributeType.values().length];
        for (int i = 0; i < primaryAttributes.length; i++) {
            primaryAttributes[i] = warrior.getSpecificBaseStatsAmount(PrimaryAttributeType.values()[i]).getAmount();
        }
        return primaryAttributes;
    }

    private double[] getSecondaryAttributes() {
        double[] secondaryAttributes = new double[SecondaryAttributeType.values().length - 1];
        for (int i = 0; i < secondaryAttributes.length; i++) {
            secondaryAttributes[i] = warrior.getSpecificSecondaryAttributes(SecondaryAttributeType.values()[i]).getAmount();
        }
        return secondaryAttributes;
    }

    @Test
    public void checkCorrectStartingPrimaryAttributes() {
        double[] primaryAttributes = getPrimaryAttributes();
        assertArrayEquals(EXPECTED_BASE_ATTRIBUTES, primaryAttributes);
    }

    @Test
    public void checkCorrectStatsOnLevelUp() throws ArgumentException {
        warrior.levelUp(1);
        double[] primaryAttributes = getPrimaryAttributes();
        assertArrayEquals(EXPECTED_LEVELUP_ATTRIBUTES, primaryAttributes);
    }

    @Test
    public void checkCorrectSecondaryStatsOnLevelUp() throws ArgumentException {
        warrior.levelUp(1);
        double[] secondaryAttributes = getSecondaryAttributes();
        assertArrayEquals(EXPECTED_LEVELUP_SECONDARY_ATTRIBUTES, secondaryAttributes);
    }

}