package equipment;

import attribute.AttributeType;
import attribute.PrimaryAttribute;
import attribute.PrimaryAttributeType;
import attribute.SecondaryAttributeType;
import character.Warrior;
import exception.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class EquipmentTest {

    private Weapon testWeapon, testBow;
    private Warrior warrior;
    private Armor testPlateBody, testClothHead;

    @BeforeEach
    public void initializeWeapons() {
        try {
            testWeapon = new Weapon("Common Axe", 1, Slot.WEAPON, WeaponType.AXE, 7, 1.1);
            testBow = new Weapon("Common Bow", 1, Slot.WEAPON, WeaponType.BOW, 12, 0.8);

        } catch (InvalidWeaponException e) {
            e.printStackTrace();
        }
    }
    @BeforeEach
    public void initializeCharacter() {
        warrior = new Warrior("Garrosh");
    }

    @BeforeEach
    public void initializeArmor() {
        PrimaryAttribute[] primaryAttributes = {
                new PrimaryAttribute(PrimaryAttributeType.VITALITY, 2),
                new PrimaryAttribute(PrimaryAttributeType.STRENGTH, 1),
                new PrimaryAttribute(PrimaryAttributeType.DEXTERITY, 0),
                new PrimaryAttribute(PrimaryAttributeType.INTELLIGENCE, 0)
        }, clothHeadAttributes = {
                new PrimaryAttribute(PrimaryAttributeType.VITALITY, 1),
                new PrimaryAttribute(PrimaryAttributeType.STRENGTH, 0),
                new PrimaryAttribute(PrimaryAttributeType.DEXTERITY, 0),
                new PrimaryAttribute(PrimaryAttributeType.INTELLIGENCE, 5)

        };
        try {
            testPlateBody = new Armor("Common Plate Body Armor", 1, Slot.BODY, ArmorType.PLATE, 10, primaryAttributes);
            testClothHead = new Armor("Common Cloth Head Armor", 1, Slot.HEAD, ArmorType.CLOTH, 10, clothHeadAttributes);
        } catch (InvalidArmorException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void equip_tooHighLevelArmor_invalidLevelException() {
        testPlateBody.setRequiredLevel(2);
        assertThrows(InvalidLevelException.class, () -> warrior.equip(testPlateBody));
    }

    @Test
    public void equip_wrongArmorType_invalidWeaponException() {
        assertThrows(InvalidArmorException.class, () -> warrior.equip(testClothHead));
    }

    @Test
    public void equip_validArmor_true() throws InvalidArmorException, InvalidLevelException {
        assertTrue(warrior.equip(testPlateBody));
    }

    @Test
    public void equip_tooHighLevelWeapon_invalidLevelException() {
        testWeapon.setRequiredLevel(2);
        assertThrows(InvalidLevelException.class, () -> warrior.equip(testWeapon));
    }

    @Test
    public void equip_wrongWeaponType_invalidWeaponException() {
        assertThrows(InvalidWeaponException.class, () -> warrior.equip(testBow));
    }

    @Test
    public void equip_validWeapon_true() throws InvalidWeaponException, InvalidLevelException {
        assertTrue(warrior.equip(testWeapon));
    }

    @Test
    public void getDamage_noWeapon_correctDPS() {
        double expectedValue = 1 * (1 + (5 * 0.01));
        assertEquals(expectedValue, warrior.getSpecificSecondaryAttributes(SecondaryAttributeType.DPS).getAmount());
    }

    @Test
    public void getDamage_validWeapon_correctDps() throws InvalidWeaponException, InvalidLevelException {
        double expectedValue = (7 * 1.1) * (1 + (5 * 0.01));
        warrior.equip(testWeapon);
        assertEquals(expectedValue, warrior.getSpecificSecondaryAttributes(SecondaryAttributeType.DPS).getAmount());
    }

    @Test
    public void getDamage_validWeaponAndArmor_correctDps() throws InvalidWeaponException, InvalidLevelException, InvalidArmorException {
        double expectedValue = (7 * 1.1) * (1 + ((5 + 1) * 0.01));
        warrior.equip(testWeapon);
        warrior.equip(testPlateBody);
        assertEquals(expectedValue, warrior.getSpecificSecondaryAttributes(SecondaryAttributeType.DPS).getAmount());
    }


}